/*
 * @Copyright: 2024 Qujunyang
 * @Author: QuJunyang
 * @Date: 2024-02-08 09:21:09
 * @LastEditTime: 2024-02-08 11:15:33
 * @LastEditors: QuJunyang
 * @FilePath: /wgpu_ws/run-wasm/src/main.rs
 * @Description: 
 */
use fs_extra::copy_items;
use fs_extra::dir::CopyOptions;
use std::path::PathBuf;

fn main() {
    let mut args = std::env::args().skip(1);
    while let Some(arg ) = args.next() {
        if arg.as_str() == "--bin" {
            let mut copy_options = CopyOptions::new();
            copy_options.overwrite = true;

            let base_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
            let mut out_dir = base_path.join("../target/wasm-example/");
            let mut res_dir = base_path.join("../");
            let mut is_need_copy = false;

            let example = args.next();
            match example.as_deref() {
                Some("tutorial1-window") => {
                    out_dir = out_dir.join("tutorial2-camera");
                    // res_dir = res_dir.join()
                    is_need_copy = true;
                }
                _ => {}
            }

            if is_need_copy {
                // let _ = copy_items(from, to, options)
            }
        }
    }

    cargo_run_wasm::run_wasm_with_css("body { margin: 0px; }");
}
