<!--
 * @Copyright: 2024 Qujunyang
 * @Author: QuJunyang
 * @Date: 2024-02-03 19:07:59
 * @LastEditTime: 2024-02-08 12:01:16
 * @LastEditors: QuJunyang
 * @FilePath: /wgpu_ws/README.md
 * @Description: 
-->
# run tutorial1-window
```shell
# 在桌面环境本地运行
cargo run --bin tutorial1-window

# 在浏览器中运行
# 需要先安装 Rust WebAssembly target
rustup target add wasm32-unknown-unknown
# 使用 WebGPU（需要使用 Chrome/Edge 113+ 或 Chrome/Edge Canary，Canary 需手动开启 WebGPU 试验功能）
cargo run-wasm --bin tutorial1-window
# 使用 WebGL 2.0
cargo run-wasm --bin tutorial1-window --features webgl
```