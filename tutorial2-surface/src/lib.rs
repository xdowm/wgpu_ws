use std::sync::Arc;

/*
 * @Copyright: 2024 Qujunyang
 * @Author: QuJunyang
 * @Date: 2024-02-08 11:37:59
 * @LastEditTime: 2024-02-08 18:52:31
 * @LastEditors: QuJunyang
 * @FilePath: /wgpu_ws/tutorial2-surface/src/lib.rs
 * @Description: 
 */
use winit::{event::WindowEvent, window::Window};

struct State {
	surface: wgpu::Surface<'static>,
	device: wgpu::Device,
	queue: wgpu::Queue,
	config: wgpu::SurfaceConfiguration, 
	size: winit::dpi::PhysicalSize<u32>,
}

impl State {
	// 创建某些 wgpu 类型需要使用异步代码
	async fn new(window: Arc<Window>) -> Self {
		todo!()
	}

	fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
		todo!()
	}

	fn input(&mut self, event: &WindowEvent) -> bool {
		todo!()
	}

	fn update(&mut self) {
		todo!()
	}

	fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
		todo!()
	}
}
